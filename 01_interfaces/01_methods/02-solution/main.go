package main

import "fmt"

type person struct {
	firstName, lastName string
}

func (p person) speak() {
	fmt.Println("My name is", p.firstName, p.lastName)
}

func main() {
	andres := person{"Andres", "Rincon"}
	andres.speak()

}
