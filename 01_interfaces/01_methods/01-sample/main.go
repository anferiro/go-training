package main

// https://goo.gl/eC7uVd 
import (
	"fmt"
)

type contactInfo struct {
	email, phone string
}
type person struct {
	firstName, lastName string
	age                 int
	contactInfo
}

func main() {
	andres := person{
		firstName: "Andres",
		lastName:  "Rincon",
		age:       38,
		contactInfo: contactInfo{
			email: "anferiro@gmail.com",
			phone: "3017855344",
		},
	}
	fmt.Println(andres)
}
