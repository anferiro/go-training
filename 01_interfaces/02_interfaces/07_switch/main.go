package main

import "fmt"

type animal interface {
	speak() string
}
type dog struct{}
type cat struct{}
type duck struct{}

func (d dog) speak() string {
	return "Woof"
}
func (c cat) speak() string {
	return "Meow"
}
func (d duck) speak() string {
	return "Quack"
}

func says(a animal) {
	switch v := a.(type) {
	case dog:
		fmt.Println("The dog says", v.speak())
	case cat:
		fmt.Println("The cat says", v.speak())
	case duck:
		fmt.Println("The duck says", v.speak())
	}
}

func main() {
	var (
		bruno  dog
		felix  cat
		donald duck
	)
	says(bruno)
	says(felix)
	says(donald)
}
