package main

import (
	"fmt"
)

type notifier interface {
	notify() string
}
type user struct {
	name, email string
}

func (u user) notify() string {

	return fmt.Sprintf("User: Sending User Email To %s<%s>\n", u.name, u.email)
}

/// polymorphic function
func sendMessage(n notifier) {
	fmt.Printf("Type: %T,Value: %v\n", n, n)
	fmt.Println(n.notify())
}
func main() {

	arincon := user{
		name:  "Andres Rincon",
		email: "anferiro@gmail.com",
	}

	sendMessage(arincon)

}
