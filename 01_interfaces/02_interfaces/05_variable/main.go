package main

import (
	"fmt"
)

type notifier interface {
	notify() string
}
type user struct {
	name, email string
}

func (u user) notify() string {

	return fmt.Sprintf("User: Sending User Email To %s<%s>\n", u.name, u.email)
}

func main() {

	arincon := user{
		name:  "Andres Rincon",
		email: "anferiro@gmail.com",
	}

	var i notifier = arincon
	fmt.Printf("(%T,%v)\n", i, i)

}
