package main

import (
	"fmt"
	"reflect"
)

type duck interface {
	fly() bool
	swim() bool
	quack() bool
}
type dog struct{}

func (d dog) fly() bool {
	return true
}

func (d dog) swim() bool {
	return true
}
func (d dog) quack() bool {
	return true
}

func main() {
	var bruno dog
	var i interface{} = bruno
	_, ok := i.(duck)
	fmt.Printf("the %v Is a Duck? %v \n", reflect.TypeOf(bruno).Name(), ok)

}
