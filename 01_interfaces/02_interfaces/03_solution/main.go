package main

import (
	"fmt"
	"reflect"
)

type animal interface {
	speak() string
}
type dog struct{}
type cat struct{}
type duck struct{}

func (d dog) speak() string {
	return "Woof"
}

func (c cat) speak() string {
	return "Meow"
}

func (d duck) speak() string {
	return "Quack"
}

func says(a animal) {
	fmt.Printf("The animal %s says %s!!!\n", reflect.TypeOf(a).Name(), a.speak())
}
func main() {
	var (
		bruno  dog
		felix  cat
		donald duck
	)
	says(bruno)
	says(felix)
	says(donald)
}
