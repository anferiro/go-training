# Embedded sample
* Create a user struct type which contains a first name and last name fields inside of it
* Create an admin type which is going to be composed by a user and also it will contain a level of acces as andditional field. 
* Create the folowwing interface
```
type Notifier interface {
	notify () string
}
```
* Implement the Notifier interface for the user type
* create a polymorphic function who recieves a notifier interface and print the message returned for the noty function
```
// Polymorphic function
func greeting (n Notifier)
```
* In the main function create a user value and an admin value and call the polymorphic function for the admin value
