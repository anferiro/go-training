package main

import (
	"fmt"
)

type person struct {
	firstName, lastName string
}
type superAgent struct {
	person
	allowedToKill bool
}

func (p person) speak() string {
	return fmt.Sprintf("Hello, My name is %v %v \n", p.firstName, p.lastName)
}

func main() {
	andres := person{"James", "Bond"}
	sa := superAgent{andres, true}
	// Method Promotion
	fmt.Println(sa.speak())

	// Field promotion
	fmt.Printf("Hello there, I am %s, %s %s \n", sa.lastName, sa.firstName, sa.lastName)

}
