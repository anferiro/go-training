package main

import (
	"fmt"
)

type Notifier interface {
	notify() string
}

type user struct {
	firstName, lastName string
}

type admin struct {
	user
	levelAccess string
}

func (u user) notify() string {
	return "hello " + u.firstName
}

// polymorphic function

func greeting(n Notifier) {
	fmt.Println(n.notify())
}
func main() {
	u := user{"Andres", "Rincon"}
	a := admin{u, "God"}
	greeting(a)

}
