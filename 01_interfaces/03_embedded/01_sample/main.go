package main

import "fmt"

type party struct {
	id, name string
}
type item struct {
	id, description string
	amount          int
	price           float64
}
type items []item
type invoce struct {
	invoiceNumber string
	customer      party
	company       party
	items
}

func (i items) getTotal() float64 {
	var total float64
	for _, v := range i {
		total += v.price
	}
	return total

}
func main() {
	invoice1 := invoce{
		company:       party{id: "8009001000", name: "Endava Inc"},
		customer:      party{id: "80006215", name: "Andrew Corner"},
		invoiceNumber: "INV-001",
		items: items{item{id: "1", description: "Web app Solution", amount: 1, price: 205},
			item{id: "2", description: "Mobile app Solution", amount: 1, price: 1000},
		},
	}

	fmt.Println("The invoice total is ", invoice1.getTotal())
}
