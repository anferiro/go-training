# Agenda
1. Getting up and go-ing
2. Building essential foundation in Go
---
# Getting up and go-ing

---
## Introduction
- Develop by google 
- It was designed to fulfill modern needs
- Go makes easier write code that is readable. 
- Simpler syntax than others

+++
## Introduction
- Golang is an impreative program. 

![enter image description here](http://i.imgur.com/4zwkmDt.gif)

---
### Better Utilization
- Go applications are statically compiled, native binaries. 
- Well-written Go programs are known for their high performance
- Better resources utilization like (CPU, RAM)
- Goroutines --> Stack 8KB

---
### Concurrency
- Concurrency is built directly in go
- It comes with goroutines and channels 
- Goroutines are litghweight threads

---
## Online Tools 
Powerfull tools to learn go
- The tour of go http://tour.golang.org
- The playground https://play.golang.org

---  
@snap[north-west]
## How to install go 
@snapend

@snap[west]
@quote[1. go to www.golang.org and download the binaries according with your OS]
![go-downloads](00_introduction/assets/go-downloads.png)
@snapend


+++
@snap[north]
@quote[2. Install the binaries]
@snapend
![Go-install](00_introduction/assets/Go-install.gif)

+++
@snap[north]
@quote[3. Verify the installation]
@snapend

```bash
cd /usr/local/go/bin 
ls
./go version
```
---
## Configuring Go
```bash
export GOROOT=/usr/local/go
export GOPATH=~/go
export GOBIN=${GOPATH}/bin
export PATH=${PATH}:${GOROOT}/bin:${GOBIN}
```

+++
### GOROOT 

Specifies where the go distribution is installed on the system

+++
### GOPATH 

Specifies our workspace were our project is going to live

```bash
cd ~
mkdir go
cd go
```
+++
```bash
#Creating our packages
mkdir src 
mkdir pkg 
mkdir bin
```
--- 
# Building essential foundation in Go

---
## Hello Gopher Demo

--- 
## Types

### Numbers
uint8, uint16, uint32, uint64, int8, int16, int32, int64, float32 and float64,
complex64 and complex128

+++
## Types

### Strings
var myString string = “hello world”

+++
## Types

### Booleans
true, false, ||, &&

---
## Variable declarations

+++
### Explicit declaration

```golang
var testa string = “hello world”
const x string = “this is a constant”
```
+++
### Implicit declaration
```golang
var test = “hello world”
test := 18.999
var (
    a = 5
    b = 10
)
```
---
## Data structures

--- 
### Arrays
```golang
array:= [5]int{1,2,3,4,5}
```
--- 
### Slices
```golang
slice:= []int{1,2,3}
//underlying array of length 5
slice1 := make([]int, 5)
//underlying array of length 5 and capacity of 10
slice2 := make([]int, 5, 10)
//increasing length from 5 to 8
slice2 := slice[:8]
```

+++
### Slices
```golang
//appending 4,5,6 elements in a new slice
slice3 := append(slice2, 4,5,6)
//copying slice4 into slice5
slice4 := []int{1,2,3}
slice5 := make([]int, 2)
copy(slice4, slice5)
```

---
### Maps
```golang
//Initializing a map
x := make(map[int]string)
y := make(map[string]map[int]int)
//deleting elements
delete(x, 1)
//checking keys
value, ok := x[0]
```
---
## Control Structures & Loop

- if
- switch - case
- for
```golang 
//Use _ to skip the use of an index
for _, value := range values {fmt.Println(value)}
```

---
## Functions
![function1](00_introduction/assets/function1.png)

+++
## Functions
![function2](00_introduction/assets/function2.png)

---?image=00_introduction/assets/structs-01.png&position=left&size=70% 70%
+++
## Structs

```bash
type person struct {
	firstName string
	lastName  string
	age   int
}
```
+++
## Structs
- No Classes (only types)
- No Inheritance
- No OOP

---?image=00_introduction/assets/avatar2.jpg&position=left&size=25% 100%

@title[Thanks]


## @size[0.6em](Thank you)

<br>

#### Andres Rincón