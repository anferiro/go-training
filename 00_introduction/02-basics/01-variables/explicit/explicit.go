package main

import "fmt"

var hello string

func main() {
	hello = "Hello Gopher!"
	fmt.Println(hello)
}
