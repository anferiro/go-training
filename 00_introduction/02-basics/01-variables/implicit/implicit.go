package main

import "fmt"

var implicit = "Hello Gopher!"
var (
	a = "Hola"
	b = "Gopher"
	c = 3
)

func main() {
	implicit1 := 2

	fmt.Println(implicit)
	fmt.Println(implicit1)
	fmt.Println(a, b, c)
}
