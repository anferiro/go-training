package main

import "fmt"

func main() {
	s := make([]int, 5, 10)
	fmt.Println(len(s))
	fmt.Println(cap(s))
	s = s[:8]
	fmt.Println(len(s))
	fmt.Println(cap(s))

}
