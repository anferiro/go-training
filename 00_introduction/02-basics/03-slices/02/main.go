package main

import "fmt"

func main() {
	s := make([]int, 5, 10)
	fmt.Println(len(s))
	fmt.Println(cap(s))
	for i := 0; i < 80; i++ {
		s = append(s, i)
		fmt.Println("Len:", len(s), "Capacity:", cap(s), "Value: ", s[i])
	}
}
